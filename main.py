#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
ZetCode PyQt5 tutorial 

In this example, we create a bit
more complicated window layout using
the QGridLayout manager. 

author: Jan Bodnar
website: zetcode.com 
last edited: January 2015
"""

import sys
from PyQt5.QtWidgets import (QWidget, QLabel, QLineEdit, 
    QTextEdit, QGridLayout, QApplication, QPushButton)
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt
import requests
import json

class Example(QWidget):
    
    def __init__(self):
        super().__init__()
        
        self.initUI()
        
    def initUI(self):
        
        grid = QGridLayout()
        #def sayHello():
            #print("Hello World")
        
        self.name = "Peter"
        self.occ = 0
        profileImgPath = "profileImg.png"
        bioTextStr = "Hi I'm Jim. I am a uni leaver looking to help a school leaver transition into uni life. I am studying at the university of Leeds within the school of Chemistry. Don't be afraid to get in touch..."
        self.watsonScore = 7.3
        watsonScore2 = str("Trust: " + str(self.watsonScore) +"/10")
        feedbackString = "Jim is a great mentor and has been a great contact to have at Leeds. I am now studying my dream course at my dream university! I made the right NextStep!!"
        messageArea1String = ""
        messageArea2String = ""
        messageArea3String = ""
        messageArea4String = ""
        messageArea5String = ""
        messageArea6String = ""
        messageArea7String = ""
        messageArea8String = ""
        messageArea9String = ""
        messageArea10String = ""
        
        def getDims():
            print(self.height())
            print(self.width())
            
        def postMessage():
            message = self.inputArea.text()
            if (self.occ == 1):
                self.name = "Beck"
                message = "\n" + message
            elif (self.occ == 2):
                self.name = "Moh"
                message = "\n" + message
            with open("text.txt", "a") as file:
                file.write(message)
                with open("text.txt", "r") as file2:
                    tempdata = file2.read()
                file2.close()
                response = requests.post("https://gateway.watsonplatform.net/personality-insights/api/v2/profile",
                auth=("bf5a3e2b-8cb4-4183-8f7a-b5467f55162e", "b0bOhDGDhiXj"),
                headers = {"content-type": "text/plain"},
                data=tempdata)
            file.close()
            my_list = response.text.split(",")
            finalVal = float(my_list[8]) * 10
            self.watsonLabel.setText("Trust: "+str(finalVal)+"/10")
            listenMessage()
            
        def listenMessage():
            
            self.messageArea1.text = ""
            self.messageArea2.text = ""
            self.messageArea3.text = ""
            self.messageArea4.text = ""
            self.messageArea5.text = ""
            self.messageArea6.text = ""
            
            #read into the list
            lines = [line.rstrip('\n') for line in open('text.txt')]
            #iterate through list most recent first
            for i in range(min(len(lines) - 1, 5), 0, -1):
                if lines[i][0] + lines[i][1] == "EX":

                    #DISPLAY AT LINE I RHS
                    if i == 4:
                        self.messageArea10.setText(lines[i])
                    elif i == 3:
                        self.messageArea8.setText(lines[i])
                    elif i == 2:
                        self.messageArea6.setText(lines[i])
                    elif i == 1:
                        self.messageArea4.setText(lines[i])
                    elif i == 0:
                        self.messageArea2.setText(lines[i])
                    
                else:
                    if i == 4:
                        self.messageArea9.setText(lines[i])
                    elif i == 3:
                        self.messageArea7.setText(lines[i])
                    elif i == 2:
                        self.messageArea5.setText(lines[i])
                    elif i == 1:
                        self.messageArea3.setText(lines[i])
                    elif i == 0:
                        self.messageArea1.setText(lines[i])
                #if message starts EX it goes on left
                #else it goes on right
                #display message at line i until i >= 15
            if self.occ == 2:
                danshere()
 
        def ammanshere():
            self.occ = 2
        
        def danshere():
            self.occ = 1
        
        #confirmButton = QPushButton('Button', self)
        #confirmButton.clicked.connect(sayHello)
        self.label1 = QLabel("Achieving NextStep", self)
        self.label2 = QLabel("Facilitating NextStep", self)
        self.schoolLeaver = QPushButton("School Leaver")
        self.schoolLeaver.clicked.connect(ammanshere)
        self.uniLeaver = QPushButton("University Leaver")
        self.uniRep = QPushButton("Univerity Representative")
        self.industryRep = QPushButton("Industry Representative")
        self.messages = QPushButton("Messages")
        self.account = QPushButton("My Account")
        self.payearn = QPushButton("Payments/Earnings")
        self.payearn.clicked.connect(danshere)
        self.settings = QPushButton("Settings")
        self.profileLabel = QLabel("My Profile")
        self.profileImg = QLabel(self)
        self.profileImg.setPixmap(QPixmap(profileImgPath).scaled(64, 64, Qt.KeepAspectRatio, Qt.FastTransformation))
        self.bioText = QLabel(bioTextStr)
        self.bioText.setWordWrap(True)
        self.stars = QLabel(self)
        self.stars.setPixmap(QPixmap("stars.png").scaled(64, 64, Qt.KeepAspectRatio, Qt.FastTransformation))
        self.watsonLabel = QLabel(watsonScore2)
        self.feedbackText = QLabel(feedbackString)
        self.feedbackText.setWordWrap(True)
        self.messageFriend1 = QLabel(self)
        self.messageFriend1.setPixmap(QPixmap("profile1.png").scaled(64, 64, Qt.KeepAspectRatio, Qt.FastTransformation))
        self.messageFriend2 = QLabel(self)
        self.messageFriend2.setPixmap(QPixmap("profile2.png").scaled(64, 64, Qt.KeepAspectRatio, Qt.FastTransformation))
        self.messageFriend3 = QLabel(self)
        self.messageFriend3.setPixmap(QPixmap("profile3.png").scaled(64, 64, Qt.KeepAspectRatio, Qt.FastTransformation))
        self.messageArea1 = QLabel(messageArea1String)
        self.messageArea2 = QLabel(messageArea2String)
        self.messageArea3 = QLabel(messageArea3String)
        self.messageArea4 = QLabel(messageArea4String)
        self.messageArea5 = QLabel(messageArea5String)
        self.messageArea6 = QLabel(messageArea6String)
        self.messageArea7 = QLabel(messageArea7String)
        self.messageArea8 = QLabel(messageArea8String)
        self.messageArea9 = QLabel(messageArea9String)
        self.messageArea10 = QLabel(messageArea10String)
        self.inputArea = QLineEdit()
        self.submitText = QPushButton("Send")
        self.submitText.clicked.connect(postMessage)
        
        grid.addWidget(self.label1, 0, 1, Qt.AlignTop)
        grid.addWidget(self.label2, 0, 3, Qt.AlignTop)
        grid.addWidget(self.schoolLeaver, 1, 0, Qt.AlignTop)
        grid.addWidget(self.uniLeaver, 1, 1, Qt.AlignTop)
        grid.addWidget(self.uniRep, 1, 2, Qt.AlignTop)
        grid.addWidget(self.industryRep, 1, 3, Qt.AlignTop)
        grid.addWidget(self.messages, 2, 0, Qt.AlignTop)
        grid.addWidget(self.account, 2, 1, Qt.AlignTop)
        grid.addWidget(self.payearn, 2, 2, Qt.AlignTop)
        grid.addWidget(self.settings, 2, 3, Qt.AlignTop)
        grid.addWidget(self.profileLabel, 3, 0, Qt.AlignHCenter)
        grid.addWidget(self.profileImg, 5, 0, Qt.AlignHCenter)
        grid.addWidget(self.bioText, 6, 0, Qt.AlignTop)
        grid.addWidget(self.stars, 4, 0, Qt.AlignHCenter)
        grid.addWidget(self.watsonLabel, 7, 0, Qt.AlignHCenter)
        grid.addWidget(self.feedbackText, 8, 0, Qt.AlignTop)
        grid.addWidget(self.messageFriend1, 3, 1, Qt.AlignHCenter)
        grid.addWidget(self.messageFriend2, 4, 1, Qt.AlignHCenter)
        grid.addWidget(self.messageFriend3, 5, 1, Qt.AlignHCenter)
        grid.addWidget(self.messageArea1, 2, 2, Qt.AlignTop)
        grid.addWidget(self.messageArea2, 2, 3, Qt.AlignTop)
        grid.addWidget(self.messageArea3, 3, 2, Qt.AlignTop)
        grid.addWidget(self.messageArea4, 3, 3, Qt.AlignTop)
        grid.addWidget(self.messageArea5, 4, 2, Qt.AlignTop)
        grid.addWidget(self.messageArea6, 4, 3, Qt.AlignTop)
        grid.addWidget(self.messageArea7, 5, 2, Qt.AlignTop)
        grid.addWidget(self.messageArea8, 5, 3, Qt.AlignTop)
        grid.addWidget(self.messageArea9, 6, 2, Qt.AlignTop)
        grid.addWidget(self.messageArea10, 6, 3, Qt.AlignTop)
        grid.addWidget(self.inputArea, 7, 2, Qt.AlignTop)
        grid.addWidget(self.submitText, 7, 3, Qt.AlignTop)
        
        
        self.setLayout(grid)
        
        self.setGeometry(0, 30, 669, 474)
        self.setWindowTitle('Review')    
        self.show()
        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())